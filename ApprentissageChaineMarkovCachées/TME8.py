#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 27 18:56:15 2017

@author: sarah
"""
import math
import numpy as np
import pickle as pkl
#from markov_tools import *
import matplotlib.pyplot as plt


def learnHMM(allx, allq, N, K, initTo1=False):
    """
    Apprend les paramètres d'un modèle HMM par comptage d'une série de séquences étiquetée
    :param allx: observations
    [[obs1, ... , obsT], [obs1, ..., obsT], ...]
         Seq 1                 Seq 2        ...
    :param allq: étiquetage
    [[s1, ... , sT], [s1, ..., sT], ...]
         Seq 1            Seq 2        ...
    :param N: nombre d'états
    :param K: nombre d'observations
    :param initTo1: initialisation à 1 (ou epsilon) pour éviter les proba 0
    :return: Pi, A, B
    Les matrices de paramétrage des HMM
    """
    if initTo1:
        eps = 1e-8
        #A = 1-np.tri(N)*eps
        A = np.ones((N,N))*eps
        B = np.ones((N,K))*eps
        #Pi = np.zeros(N)
        Pi = np.ones(N)*eps
    else:
        A = np.zeros((N,N))
        B = np.zeros((N,K))
        Pi = np.zeros(N)
    for x,q in zip(allx,allq):
        Pi[int(q[0])] += 1
        for i in range(len(q)-1):
            A[int(q[i]),int(q[i+1])] += 1
            B[int(q[i]),int(x[i])] += 1
        B[int(q[-1]),int(x[-1])] += 1 # derniere transition
    A = A/np.maximum(A.sum(1).reshape(N,1),1) # normalisation
    B = B/np.maximum(B.sum(1).reshape(N,1),1) # normalisation
    Pi = Pi/Pi.sum()
    return Pi , A, B


def calc_log_pobs(x, Pi,A,B):

    #Algorithme alpha de calcul de la vraisemblance d'une séquence d'observations sachant le modèle
    #p(x | lambda)
    #:param x: [obs1, ... , obsT] (UNE séquence)
    #:param Pi: param HMM
    #:param A: param HMM
    #param B: param HMM
    #:return: log(p(x | lambda))
    T = len(x)
    N = len(Pi)
    alpha = np.zeros((N,T))
    omega = np.zeros(T)
    alpha[:,0] = Pi * B[:,x[0]]
    omega[0] = alpha[:,0].sum()
    alpha[:,0] /= omega[0]
    for t in range(1,T):
        alpha[:,t] = alpha[:,t-1].reshape(1,N).dot(A) * B[:,x[t]]
        omega[t] = alpha[:,t].sum()
        alpha[:,t] /= omega[t]
    return alpha, np.log(omega).sum() #, omega


def backward(x,Pi, A, B) :
    nbEtats = len(A) #N
    nbE = len(x)
    beta = np.zeros(( nbEtats, nbE))
    T = nbE - 1
    for i in range(nbEtats):
        beta[i][T] = 1

    omega = np.zeros(nbE)
    omega[0] = beta[:,T].sum()
    beta[:,T] /= omega[0]
    t = T - 1
    while t >= 0 :
        beta[:,t] = beta[:,t+1].reshape(1,nbEtats).dot(A) * B[:,x[t+1]]
        omega[t] = beta[:,t].sum()
        beta[:,t] /= omega[t]
        t -= 1
    return beta, np.log(omega).sum() #, omega

def viterbi(x,Pi,A,B):
    """
    Algorithme de Viterbi (en log) pour le décodage des séquences d'états:
    argmax_s p(x, s | lambda)
    :param x: [obs1, ... , obsT] (UNE séquence)
    :param Pi: param HMM
    :param A: param HMM
    :param B: param HMM
    :return: s (la séquence d'état la plus probable), estimation de p(x|lambda)
    """
    T = len(x)
    N = len(Pi)
    logA = np.log(A)
    logB = np.log(B)
    logdelta = np.zeros((N,T))
    psi = np.zeros((N,T), int)
    S = np.zeros(T, int)
    logdelta[:,0] = np.log(Pi) + logB[:,x[0]]
    #forward
    for t in range(1,T):
        logdelta[:,t] = (logdelta[:,t-1].reshape(N,1) + logA).max(0) + logB[:,x[t]]
        psi[:,t] = (logdelta[:,t-1].reshape(N,1) + logA).argmax(0)
    # backward
    logp = logdelta[:,-1].max()
    S[T-1] = logdelta[:,-1].argmax()
    for i in range(2,T+1):
        S[int(T-i)] = psi[S[T-i+1],int(T-i+1)]
    return S, logp


def viterbi_contraintes(x,Pi,A,B, states):
    """
    Algorithme de Viterbi (en log) pour le décodage des séquences d'états:
    argmax_s p(x, s | lambda)
    :param x: [obs1, ... , obsT] (UNE séquence)
    :param Pi: param HMM
    :param A: param HMM
    :param B: param HMM
    :return: s (la séquence d'état la plus probable), estimation de p(x|lambda)
    """
    T = len(x)
    N = len(Pi)
    logA = np.log(A)
    logB = np.log(B)
    logdelta = np.zeros((N,T))
    psi = np.zeros((N,T), int)
    S = np.zeros(T, int)
    logdelta[:,0] = np.log(Pi) + logB[:,x[0]]
    #forward
    for t in range(1,T):
        logdelta[:,t] = (logdelta[:,t-1].reshape(N,1) + logA).max(0) + logB[:,x[t]]
        psi[:, t] = (logdelta[:, t - 1].reshape(N, 1) + logA).argmax(0)
        if states[t] != -1:
            logdelta[[i for i in range(N) if i != states[t]], t] = -1e8
    # backward
    logp = logdelta[:,-1].max()
    S[T-1] = logdelta[:,-1].argmax()
    for i in range(2,T+1):
        S[T-i] = psi[S[T-i+1],int(T-i+1)]
    return S, logp



############################################## PARTIE I :

with open('genome_genes.pkl.txt', 'rb') as f:
    data = pkl.load(f, encoding='latin1')

Xgenes  = data.get("genes") #Les genes, une array de arrays

Genome = data.get("genome") #le premier million de bp de Coli

Annotation = data.get("annotation") ##l'annotation sur le genome
##0 = non codant, 1 = gene sur le brin positif

### Quelques constantes
DNA = ["A", "C", "G", "T"]
stop_codons = ["TAA", "TAG", "TGA"]

# A = 0, C= 2, G=2 et T = 3
print("\nQUESTION 1 :")
print(Xgenes[0][:6])
print(Xgenes[0][-6:])



############################################## PARTIE II :

a = 1/200
print("\nQUESTION 2 :")
print("SOUS-QUESTION 2-1 :")
print("a = ", a)

b = 0
for el in Xgenes :
    b += len(el)
b /= len(Xgenes)
b = 1/b
print("\nSOUS-QUESTION 2-2 :")
print("b = ", b)

def Binter(Annotation) :
    tmp = []
    for i in range(len(Annotation)) :
        tmp.append(Genome[i])

    A = tmp.count(0) / len(tmp)
    C = tmp.count(1) / len(tmp)
    G = tmp.count(2) / len(tmp)
    T = tmp.count(3) / len(tmp)
    return np.array([A,C,G,T])

print("\nSOUS-QUESTION 2-3 :")
Binter = Binter(Annotation)
print(Binter)

def distribCodons(P,L, genes, res):
    for el in genes :
        i = P
        while i < len(el) - 3 :
            if el[i] == 0 :
                res[L][0] += 1
            if el[i] == 1 :
                res[L][1] += 1
            if el[i] == 2 :
                res[L][2] += 1
            if el[i] == 3 :
                res[L][3] += 1
            i += 3
    s = np.sum(res[L])
    i = 0
    for el in res[L]:
        res[L][i] = el/s
        i += 1
    return res


def Bgene(genes) :
    res = np.zeros((3,4))
    Ligne = 0
    for position in range(3,6):
        res = distribCodons(position, Ligne, genes, res)
        Ligne += 1
    return res

print("\nSOUS-QUESTION 2-4 :")
Bgene = Bgene(Xgenes)
print(Bgene)



n_states_m1 = 4
B = np.vstack((Binter, Bgene))
Pi = np.array([1, 0, 0, 0]) ##on commence dans l'intergenique
A = np.array([[1-a, a  , 0, 0],
                 [0  , 0  , 1, 0],
                 [0  , 0  , 0, 1],
                 [b  , 1-b, 0, 0 ]])


# pour utiliser le modèle plus loin:
vsbce, pred = viterbi(Genome,Pi,A,B)
sp = vsbce

for i in range(sp.shape[0]) :
    if sp[i] >= 1 :
        sp[i] = 1.
#sp[np.where(sp>=1.)] = 1.
percpred1 = np.sum(sp == Annotation) / len(Annotation)

print("\nSOUS-QUESTION 2-5 :")
print(percpred1)

print("\nSOUS-QUESTION 2-6 : \nGraphique de la classification via CMC comparé aux annotations réelles :")
x = np.linspace(0., 6000., num = 6000)
y1 = Annotation[:6000]
y2 = vsbce[:6000]
plt.figure()
plt.plot(x, y1)
plt.plot(x, y2)
plt.plot(x, y1, label="Annotation")
plt.plot(x, y2, label="Classication CMC")
plt.legend()
plt.show()


############################################## PARTIE III :

Pi = np.array([1  , 0  , 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]) ##on commence dans l'intergenique
A = np.array([[1-a, 0  , 0, 0, a*0.83, a*0.14, a*0.003, 0, 0, 0, 0, 0, 0, 0],
              [0  , 0  , 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0  , 0  , 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0  , 1-b, 0, 0, 0, 0, 0, 0, 0, b, 0, 0, 0, 0],
              [0  , 0  , 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
              [0  , 0  , 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
              [0  , 0  , 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
              [0  , 0  , 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
              [0  , 1  , 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0  , 0  , 0, 0, 0, 0, 0, 0, 0, 0, 0.5, 0.5, 0, 0],
              [0  , 0  , 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
              [0  , 0  , 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.5, 0.5],
              [1  , 0  , 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [1  , 0  , 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]])
B = np.array([[ 0.242654,    0.248799,    0.265139,    0.243408  ],
 [ 0.25120572,  0.24063005,  0.35462469,  0.15353954],
 [ 0.29302417,  0.23050332,  0.18223301,  0.2942395 ],
 [ 0.18018394,  0.26191285,  0.29654511,  0.2613581 ],
 [ 1,  0,  0,  0 ],
 [ 0,  0,  1,  0 ],
 [ 0,  0,  0,  1 ],
 [ 0,  0,  0,  1 ],
 [ 0,  0,  1,  0 ],
 [ 0,  0,  0,  1 ],
 [ 0,  0,  1,  0 ],
 [ 1,  0,  0,  0 ],
 [ 1,  0,  0,  0 ],
 [ 0,  0,  1,  0 ]])


vsbce, pred = viterbi(Genome,Pi,A,B)
sp = vsbce
for i in range(sp.shape[0]) :
    if sp[i] >= 1 :
        sp[i] = 1.
#sp[np.where(sp>=1.)] = 1.
percpred1 = np.sum(sp == Annotation) / len(Annotation)

print("\nQUESTION 3 :")
print("SOUS-QUESTION 3-1 :")
#print(A)

print("\nSOUS-QUESTION 3-2 :")
#print(B)

print("\nSOUS-QUESTION 3-3 :")
print(percpred1)

print("\nSOUS-QUESTION 3-4 : \nGraphique de la classification via CMC comparé aux annotations réelles :")
x = np.linspace(0., 6000., num = 6000)
y1 = Annotation[:6000]
y2 = vsbce[:6000]
plt.figure()
plt.plot(x, y1)
plt.plot(x, y2)
plt.plot(x, y1, label="Annotation")
plt.plot(x, y2, label="Classication CMC")
plt.legend()
plt.show()


############################################## PARTIE IV :

#model3 = hmm.MultinomialHMM(n_s_m2, n_iter = 100, thresh = 1e-6,   #les paramètres de la
       #                     params = "es")    #l'estimation est faite pour les emission "e" et le start "s"
#model3._set_transmat(A_m2)
#model3.fit(G)

#model3.transmat_  # afficher la matrice estimée


def BW_S(X, Pi0, A0, B0) :
    Pi = Pi0
    A = A0
    B = B0
    alls = X.copy()
    for i in range(5):
        LL = 0
        s, p = viterbi(X, Pi, A, B)
        LL += p
        Pi, A, B = learnHMM([X], [s], len(A),len(B[0]))
        print("Les améliorations de la vraisemblance au fur et à mesure:")
        print(LL)

"""
On a essayé de faire le Baum Welch original mais
il ne fonctionne pas véritablement bien
Cependant, le squelette semble à peu pres correcte
"""

def Baum_Welch_O(x, N, A, B, Pi):
    # Initialisation :
    Amat = A
    Bmat = B
    Pimat = Pi
    theta = np.zeros((len(A), len(A), len(x)))
    gamma = np.zeros((len(A), len(x)))

    while True:
        print("passe")
        oldA = Amat
        oldB = t = 0
        oldPi = Pimat
        oldv, oldp = viterbi(x,oldPi,oldA,oldB)
        #Forward
        alpha, va = calc_log_pobs(x, oldPi, oldA, oldB)
        #Backward
        beta, vb = backward(x, oldPi, oldA, oldB)

        # On rempli les matrices theta et gamma :
        L_po = []
        for t in range(len(x)):
            PO = 0
            for i in range(N) :
                PO += alpha[i][t] * beta[i][t]
            L_po.append(PO)


        for t in range (len(x)-1) :
            for i in range(N) :
                somme = 0
                for j in range(N) :
                    theta[i][j][t] = (alpha[i][t] * beta[j][t+1] * oldA[i][j] * oldB[j][x[t+1]]) / L_po[t]
                    somme += theta[i][j][t]
                gamma[i][t] =  somme

        # On ré-estime les maptrices Pi, A et B :
        for i in range(N) :
            Pimat[i] = gamma[i][0]#/len(x)

        # On réestime A
        i = 0
        for i in range(N):
            for j in range(N):
                Amat[i][j] = np.sum(theta[i,j, :] ) / np.sum(gamma[i, :])

        # On réestime B
        for j in range(len(B)):
            for k in range(len(B[0])) :
                somme = 0
                for t in range(1,len(x)):
                    if x[t] == k :
                        somme += gamma[j][t]

                Bmat[j][k] = somme / np.sum(gamma[j, :])
            #Bmat = Bmat / np.sum(Bmat,1)
        v, p = viterbi(x,Pimat,Amat,Bmat)
        if math.fabs(p - oldp) < 0.01 :
            print(p)
            print(oldp)
            break

    return Amat, Bmat


BW_S(Genome, Pi, A, B)
