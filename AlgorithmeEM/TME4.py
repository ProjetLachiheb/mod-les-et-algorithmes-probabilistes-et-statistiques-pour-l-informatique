# -*- coding: utf-8 -*-
"""
Created on Mon Oct  9 21:19:44 2017

@author: Sarah
"""

# -*- coding: utf-8 -*-

import os
os.chdir("C:\\Users\\sarah\\Desktop\\Master\\MAPSI\\TME4")

import math

import numpy as np
from math import *
from pylab import *

def read_file ( filename ):
    """
    Lit le fichier contenant les données du geyser Old Faithful
    """
    # lecture de l'en-tête
    infile = open ( filename, "r" )
    for ligne in infile:
        if ligne.find ( "eruptions waiting" ) != -1:
            break

    # ici, on a la liste des temps d'éruption et des délais d'irruptions
    data = []
    for ligne in infile:
        nb_ligne, eruption, waiting = [ float (x) for x in ligne.split () ]
        data.append ( eruption )
        data.append ( waiting )
    infile.close ()

    # transformation de la liste en tableau 2D
    data = np.asarray ( data )
    data.shape = ( int ( data.size / 2 ), 2 )

    return data


data = read_file ( "2015_tme4_faithful.txt" )

def normale_bidim (x, z, q) :
    partie1 = 1 / (2*math.pi*q[2]*q[3]*math.sqrt(1-q[4]**2))
    partie2 = -(1/(2*(1-q[4]**2)))
    partie3 = (((x-q[0]) / q[2])**2 - (2*q[4]*(((x - q[0])*(z-q[1])) / (q[2]*q[3])) ) + ((z - q[1]) / q[3])**2)
    return partie1 * math.exp (partie2 * partie3)

#print(normale_bidim ( 1, 2, (1.0,2.0,3.0,4.0,0) ))
#print(normale_bidim ( 1, 0, (1.0,2.0,1.0,2.0,0.7) ))

import matplotlib.pyplot as plt

def dessine_1_normale ( params ):
    # récupération des paramètres
    mu_x, mu_z, sigma_x, sigma_z, rho = params

    # on détermine les coordonnées des coins de la figure
    x_min = mu_x - 2 * sigma_x
    x_max = mu_x + 2 * sigma_x
    z_min = mu_z - 2 * sigma_z
    z_max = mu_z + 2 * sigma_z

    # création de la grille
    x = np.linspace ( x_min, x_max, 100 )
    z = np.linspace ( z_min, z_max, 100 )
    X, Z = np.meshgrid(x, z)

    # calcul des normales
    norm = X.copy ()
    for i in range ( x.shape[0] ):
        for j in range ( z.shape[0] ):
            norm[i,j] = normale_bidim ( x[i], z[j], params )

    # affichage
    fig = plt.figure ()
    plt.contour ( X, Z, norm, cmap=cm.autumn )
    plt.show ()
    
#dessine_1_normale ( (-3.0,-5.0,3.0,2.0,0.7) )
#dessine_1_normale ( (-3.0,-5.0,3.0,2.0,0.2) )

def dessine_normales ( data, params, weights, bounds, ax ):
    # récupération des paramètres
    mu_x0, mu_z0, sigma_x0, sigma_z0, rho0 = params[0]
    mu_x1, mu_z1, sigma_x1, sigma_z1, rho1 = params[1]

    # on détermine les coordonnées des coins de la figure
    x_min = bounds[0]
    x_max = bounds[1]
    z_min = bounds[2]
    z_max = bounds[3]

    # création de la grille
    nb_x = nb_z = 100
    x = np.linspace ( x_min, x_max, nb_x )
    z = np.linspace ( z_min, z_max, nb_z )
    X, Z = np.meshgrid(x, z)

    # calcul des normales
    norm0 = np.zeros ( (nb_x,nb_z) )
    for j in range ( nb_z ):
        for i in range ( nb_x ):
            norm0[j,i] = normale_bidim ( x[i], z[j], params[0] )# * weights[0]
    norm1 = np.zeros ( (nb_x,nb_z) )
    for j in range ( nb_z ):
        for i in range ( nb_x ):
             norm1[j,i] = normale_bidim ( x[i], z[j], params[1] )# * weights[1]

    # affichages des normales et des points du dataset
    ax.contour ( X, Z, norm0, cmap=cm.winter, alpha = 0.5 )
    ax.contour ( X, Z, norm1, cmap=cm.autumn, alpha = 0.5 )
    for point in data:
        ax.plot ( point[0], point[1], 'k+' )


def find_bounds ( data, params ):
    # récupération des paramètres
    mu_x0, mu_z0, sigma_x0, sigma_z0, rho0 = params[0]
    mu_x1, mu_z1, sigma_x1, sigma_z1, rho1 = params[1]

    # calcul des coins
    x_min = min ( mu_x0 - 2 * sigma_x0, mu_x1 - 2 * sigma_x1, data[:,0].min() )
    x_max = max ( mu_x0 + 2 * sigma_x0, mu_x1 + 2 * sigma_x1, data[:,0].max() )
    z_min = min ( mu_z0 - 2 * sigma_z0, mu_z1 - 2 * sigma_z1, data[:,1].min() )
    z_max = max ( mu_z0 + 2 * sigma_z0, mu_z1 + 2 * sigma_z1, data[:,1].max() )

    return ( x_min, x_max, z_min, z_max )


# affichage des données : calcul des moyennes et variances des 2 colonnes
mean1 = data[:,0].mean ()
mean2 = data[:,1].mean ()
std1  = data[:,0].std ()
std2  = data[:,1].std ()

# les paramètres des 2 normales sont autour de ces moyennes
params = np.array ( [(mean1 - 0.2, mean2 - 1, std1, std2, 0),
                     (mean1 + 0.2, mean2 + 1, std1, std2, 0)] )
weights = np.array ( [0.4, 0.6] )
bounds = find_bounds ( data, params )

# affichage de la figure
"""
fig = plt.figure ()
ax = fig.add_subplot(111)
dessine_normales ( data, params, weights, bounds, ax )
plt.show ()
"""
"_________________5. EM : l'étape E___________________"



def Q_i(data, current_params, current_weights) :
    E = np.zeros((data.shape))
    for i in range (data.shape[0]) :
        alpha_0 =  current_weights[0] * normale_bidim(data[i][0], data[i][1],current_params[0])
        alpha_1 =  current_weights[1] * normale_bidim(data[i][0], data[i][1],current_params[1])
        E[i][0] = alpha_0 / (alpha_0 + alpha_1)
        E[i][1] = alpha_1 / (alpha_0 + alpha_1)
    return E

#current_params = np.array ( [(mu_x, mu_z, sigma_x, sigma_z, rho),   # params 1ère loi normale
#                             (mu_x, mu_z, sigma_x, sigma_z, rho)] ) # params 2ème loi normale
current_params = np.array([[ 3.28778309, 69.89705882, 1.13927121, 13.56996002, 0. ],
                           [ 3.68778309, 71.89705882, 1.13927121, 13.56996002, 0. ]])

# current_weights = np.array ( [ pi_0, pi_1 ] )
current_weights = np.array ( [ 0.5, 0.5 ] )

T = Q_i ( data, current_params, current_weights )

current_params = np.array([[ 3.2194684, 67.83748075, 1.16527301, 13.9245876,  0.9070348 ],
                           [ 3.75499261, 73.9440348, 1.04650191, 12.48307362, 0.88083712]])
current_weights = np.array ( [ 0.49896815, 0.50103185] )
T = Q_i ( data, current_params, current_weights )

"__________________6. EM : l'étape M_______________________"
import math 

def M_step(data, Q, current_params, current_weights) : 
    sum_Qi = Q.sum(0)
    Q_io, Q_i1 = (sum_Qi[0], sum_Qi[1])
    # Calcul de PI :
    pi = np.zeros((2,1))
    pi[0] = Q_io / (Q_io + Q_i1)
    pi[1] = Q_i1 / (Q_io + Q_i1)
    # Calcul des parametres mu:
    sum_Qyo_x = Q[:,0].dot(data[:,0])
    sum_Qy1_x = Q[:,1].dot(data[:,0])
    sum_Qyo_z = Q[:,0].dot(data[:,1])
    sum_Qy1_z = Q[:,1].dot(data[:,1])
    mu_xo = sum_Qyo_x / Q_io
    mu_x1 = sum_Qy1_x / Q_i1
    mu_zo = sum_Qyo_z / Q_io 
    mu_z1 = sum_Qy1_z / Q_i1
    # Calcul des parametres psi :
    psi_xo =(math.sqrt(Q[:,0].dot(((data-mu_xo)**2))[0] / Q_io))
    psi_x1 =(math.sqrt(Q[:,1].dot(((data-mu_x1)**2))[0] / Q_i1))
    psi_zo =(math.sqrt(Q[:,0].dot(((data-mu_zo)**2))[1] / Q_io))
    psi_z1 =(math.sqrt(Q[:,1].dot(((data-mu_z1)**2))[1] / Q_i1))
    # Calcule des parametres rho :
    rho_o = (Q[:,0].dot((((data[:,0]-mu_xo)*(data[:,1]-mu_zo)).T / (psi_xo*psi_zo)))) / Q_io
    rho_1 = (Q[:,1].dot((((data[:,0]-mu_x1)*(data[:,1]-mu_z1)).T / (psi_x1*psi_z1)))) / Q_i1
    return np.array([[mu_xo, mu_zo, psi_xo, psi_zo, rho_o],[mu_x1, mu_z1, psi_x1, psi_z1, rho_1]]), pi
    
"""
 (array([[ 2.33418412, 58.06784269, 0.74224878, 10.17591317, 0.82161824],
         [ 4.33880698, 80.36132657, 0.37819574,  5.71033527, 0.3008745 ]]),
  array( [ 0.42453067,  0.57546933 ] ) )
"""


current_params = array([(2.51460515, 60.12832316, 0.90428702, 11.66108819, 0.86533355),
                        (4.2893485,  79.76680985, 0.52047055,  7.04450242, 0.58358284)])
current_weights = array([ 0.45165145,  0.54834855])
Q = Q_i ( data, current_params, current_weights )

pi, params = M_step ( data, Q, current_params, current_weights )

"____________________7. Algorithme EM : mise au point_____________________"

def EM (data, params, weights) :
    Q =  Q_i ( data, params, weights )
    p, w = M_step(data, Q, params, weights)
    return p, w

mean1 = data[:,0].mean ()
mean2 = data[:,1].mean ()
std1  = data[:,0].std ()
std2  = data[:,1].std ()
params = np.array ( [(mean1 - 0.2, mean2 - 1, std1, std2, 0),
                     (mean1 + 0.2, mean2 + 1, std1, std2, 0)] )
weights = np.array ( [ 0.5, 0.5 ] )
bounds = find_bounds ( data, params )


for i in range (18) :
    params, weights = EM (data, params, weights)

fig = plt.figure ()
ax = fig.add_subplot(111)
ax.text(5, 50, 'step = 18')
dessine_normales (data, params, weights, bounds, ax)
plt.show ()

"__________________8. Algorithme EM : version finale et animation______________"


n = 20
m = 2
res_EM = [0] * n
for i in range (n) :
    res_EM[i] = [0]*m
    
for i in range (n) :
    for j in range (m) :
        if j == 0 :
            res_EM[i][j] = np.zeros((2,5))
        else :
           res_EM[i][j] = np.zeros((1,2))


res_EM[0][0] = params
res_EM[0][1] = weights
for i in range(19) : 
    res_EM[i+1][0], res_EM[i+1][1] = EM(data, res_EM[i][0], res_EM[i][1])

# calcul des bornes pour contenir toutes les lois normales calculées
def find_video_bounds ( data, res_EM ):
    bounds = np.asarray ( find_bounds ( data, res_EM[0][0] ) )
    for param in res_EM:
        new_bound = find_bounds ( data, param[0] )
        for i in [0,2]:
            bounds[i] = min ( bounds[i], new_bound[i] )
        for i in [1,3]:
            bounds[i] = max ( bounds[i], new_bound[i] )
    return bounds

bounds = find_video_bounds (data, res_EM)

import matplotlib.animation as animation

# création de l'animation : tout d'abord on crée la figure qui sera animée


# la fonction appelée à chaque pas 
# la fonction appelée à chaque pas de temps pour créer l'animation
def animate (i):
    ax.cla ()
    dessine_normales (data, res_EM[i][0], res_EM[i][1], bounds, ax)
    ax.text(5, 40, 'step = ' + str ( i ))
    print ('step animate = %d' % ( i ))

"""
Quand nous faisons l'animation dans le terminal cela fonctionne, cependant, sous syder
nous avons une erreur à cause de Syder donc nous avons commenté pour ne pas perturber 
l'execution mais vous pouvez decommenter afin de vérifier dans votre terminal. 
Je vous remercie.
"""
# exécution de l'animation

#fig = plt.figure () 
#ax = fig.gca (xlim=(bounds[0], bounds[1]), ylim=(bounds[2], bounds[3]))
# exécution de l'animation
#anim = animation.FuncAnimation(fig, animate, frames = len(res_EM), interval=500)
#plt.show ()
#python c:\Users\sarah\Desktop\Master\MAPSI\TME4\TME4.py




"____________________K_means______________________"
"""
def init(data):
    Qbis = np.zeros((data.shape[0], data.shape[1]))
    for i in range (data.shape[0]) :
        if np.random.random() > 0.5 :
            Qbis[i][0] = 1
        else:
            Qbis[i][1] = 1
    return Qbis

def initProto(Q):        
    proto1 = np.array([0,0])
    for i in range (data.shape[0]) :
        if Qbis[i][0] == 1 :
            proto1[0] += data[i][0]
            proto1[1] += data[i][1]
    proto1[0] /= sum(Qbis[:,0])
    proto1[1] /= sum(Qbis[:,0])

    proto2 = np.array([0,0])
    for i in range (data.shape[0]) :
        if Qbis[i][1] == 1 :
            proto2[0] += data[i][0]
            proto2[1] += data[i][1]
    proto2[0] /= sum(Qbis[:,1])
    proto2[1] /= sum(Qbis[:,1])
    
    return proto1, proto2


def phase2 (data, proto1, proto2):
    # Prototype 1 :
    tab_proto1 = np.zeros((data.shape[0],1))
    # Calcule distance euclidienne :
    for i in range (tab_proto1.shape[0]):
        tab_proto1[i] = sqrt((proto1[0]-proto1[1])**2 + (data[i,0]-data[i,1])**2)
    

    # Prototype 1 :
    tab_proto2 = np.zeros((data.shape[0],1))
    # Calcule distance euclidienne :
    for i in range (tab_proto1.shape[0]):
        tab_proto2[i] = sqrt((proto2[0]-proto2[1])**2 + (data[i,0]-data[i,1])**2)

    # Creation de cluster :
    Q = np.zeros((data.shape[0], data.shape[1]))
    for i in range (data.shape[0]) :
        if tab_proto1[i] < tab_proto2[i] :
            Q[i][0] = 1
        else:
            Q[i][1] = 1
    return(Q)


def K_means(data):
    Qbis = init(data)
    proto1, proto2 = initProto(Qbis)
    while round(proto1[0],1) != round(proto2[0],1) :
        Q = phase2(data, proto1, proto2)
        proto1, proto2 = initProto(Q)
    return proto1, proto2
    
print(K_means(data))

"""





















