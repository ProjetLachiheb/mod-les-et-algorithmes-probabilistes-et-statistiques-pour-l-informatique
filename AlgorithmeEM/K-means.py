# -*- coding: utf-8 -*-
"""
Created on Tue Oct 17 11:08:08 2017

@author: Sarah
"""

import os
os.chdir("C:\\Users\\sarah\\Desktop\\Master\\MAPSI\\TME4")


import numpy as np
from math import *
from pylab import *

def read_file ( filename ):
    """
    Lit le fichier contenant les données du geyser Old Faithful
    """
    # lecture de l'en-tête
    infile = open ( filename, "r" )
    for ligne in infile:
        if ligne.find ( "eruptions waiting" ) != -1:
            break

    # ici, on a la liste des temps d'éruption et des délais d'irruptions
    data = []
    for ligne in infile:
        nb_ligne, eruption, waiting = [ float (x) for x in ligne.split () ]
        data.append ( eruption )
        data.append ( waiting )
    infile.close ()

    # transformation de la liste en tableau 2D
    data = np.asarray ( data )
    data.shape = ( int ( data.size / 2 ), 2 )

    return data


data = read_file ( "2015_tme4_faithful.txt" )

def normale_bidim (x, z, q) :
    partie1 = 1 / (2*math.pi*q[2]*q[3]*math.sqrt(1-q[4]**2))
    partie2 = -(1/(2*(1-q[4]**2)))
    partie3 = (((x-q[0]) / q[2])**2 - (2*q[4]*(((x - q[0])*(z-q[1])) / (q[2]*q[3])) ) + ((z - q[1]) / q[3])**2)
    return partie1 * math.exp (partie2 * partie3)

#print(normale_bidim ( 1, 2, (1.0,2.0,3.0,4.0,0) ))
#print(normale_bidim ( 1, 0, (1.0,2.0,1.0,2.0,0.7) ))

import matplotlib.pyplot as plt

def dessine_1_normale ( params ):
    # récupération des paramètres
    mu_x, mu_z, sigma_x, sigma_z, rho = params

    # on détermine les coordonnées des coins de la figure
    x_min = mu_x - 2 * sigma_x
    x_max = mu_x + 2 * sigma_x
    z_min = mu_z - 2 * sigma_z
    z_max = mu_z + 2 * sigma_z

    # création de la grille
    x = np.linspace ( x_min, x_max, 100 )
    z = np.linspace ( z_min, z_max, 100 )
    X, Z = np.meshgrid(x, z)

    # calcul des normales
    norm = X.copy ()
    for i in range ( x.shape[0] ):
        for j in range ( z.shape[0] ):
            norm[i,j] = normale_bidim ( x[i], z[j], params )

    # affichage
    fig = plt.figure ()
    plt.contour ( X, Z, norm, cmap=cm.autumn )
    plt.show ()
    
#dessine_1_normale ( (-3.0,-5.0,3.0,2.0,0.7) )
#dessine_1_normale ( (-3.0,-5.0,3.0,2.0,0.2) )

def dessine_normales ( data, params, weights, bounds, ax ):
    # récupération des paramètres
    mu_x0, mu_z0, sigma_x0, sigma_z0, rho0 = params[0]
    mu_x1, mu_z1, sigma_x1, sigma_z1, rho1 = params[1]

    # on détermine les coordonnées des coins de la figure
    x_min = bounds[0]
    x_max = bounds[1]
    z_min = bounds[2]
    z_max = bounds[3]

    # création de la grille
    nb_x = nb_z = 100
    x = np.linspace ( x_min, x_max, nb_x )
    z = np.linspace ( z_min, z_max, nb_z )
    X, Z = np.meshgrid(x, z)

    # calcul des normales
    norm0 = np.zeros ( (nb_x,nb_z) )
    for j in range ( nb_z ):
        for i in range ( nb_x ):
            norm0[j,i] = normale_bidim ( x[i], z[j], params[0] )# * weights[0]
    norm1 = np.zeros ( (nb_x,nb_z) )
    for j in range ( nb_z ):
        for i in range ( nb_x ):
             norm1[j,i] = normale_bidim ( x[i], z[j], params[1] )# * weights[1]

    # affichages des normales et des points du dataset
    ax.contour ( X, Z, norm0, cmap=cm.winter, alpha = 0.5 )
    ax.contour ( X, Z, norm1, cmap=cm.autumn, alpha = 0.5 )
    for point in data:
        ax.plot ( point[0], point[1], 'k+' )


def find_bounds ( data, params ):
    # récupération des paramètres
    mu_x0, mu_z0, sigma_x0, sigma_z0, rho0 = params[0]
    mu_x1, mu_z1, sigma_x1, sigma_z1, rho1 = params[1]

    # calcul des coins
    x_min = min ( mu_x0 - 2 * sigma_x0, mu_x1 - 2 * sigma_x1, data[:,0].min() )
    x_max = max ( mu_x0 + 2 * sigma_x0, mu_x1 + 2 * sigma_x1, data[:,0].max() )
    z_min = min ( mu_z0 - 2 * sigma_z0, mu_z1 - 2 * sigma_z1, data[:,1].min() )
    z_max = max ( mu_z0 + 2 * sigma_z0, mu_z1 + 2 * sigma_z1, data[:,1].max() )

    return ( x_min, x_max, z_min, z_max )


# affichage des données : calcul des moyennes et variances des 2 colonnes
mean1 = data[:,0].mean ()
mean2 = data[:,1].mean ()
std1  = data[:,0].std ()
std2  = data[:,1].std ()

# les paramètres des 2 normales sont autour de ces moyennes
params = np.array ( [(mean1 - 0.2, mean2 - 1, std1, std2, 0),
                     (mean1 + 0.2, mean2 + 1, std1, std2, 0)] )
weights = np.array ( [0.4, 0.6] )
bounds = find_bounds ( data, params )

# affichage de la figure

fig = plt.figure ()
ax = fig.add_subplot(111)
dessine_normales ( data, params, weights, bounds, ax )
plt.show ()

def init(data):
    Qbis = np.zeros((data.shape[0], data.shape[1]))
    for i in range (data.shape[0]) :
        if np.random.random() > 0.5 :
            Qbis[i][0] = 1
        else:
            Qbis[i][1] = 1
    return Qbis

def initProto(Q):        
    proto1 = np.array([0,0])
    for i in range (data.shape[0]) :
        if Qbis[i][0] == 1 :
            proto1[0] += data[i][0]
            proto1[1] += data[i][1]
    proto1[0] /= sum(Qbis[:,0])
    proto1[1] /= sum(Qbis[:,0])

    proto2 = np.array([0,0])
    for i in range (data.shape[0]) :
        if Qbis[i][1] == 1 :
            proto2[0] += data[i][0]
            proto2[1] += data[i][1]
    proto2[0] /= sum(Qbis[:,1])
    proto2[1] /= sum(Qbis[:,1])
    
    return proto1, proto2


def phase2 (data, proto1, proto2):
    # Prototype 1 :
    tab_proto1 = np.zeros((data.shape[0],1))
    # Calcule distance euclidienne :
    for i in range (tab_proto1.shape[0]):
        tab_proto1[i] = sqrt((proto1[0]-proto1[1])**2 + (data[i,0]-data[i,1])**2)
    

    # Prototype 1 :
    tab_proto2 = np.zeros((data.shape[0],1))
    # Calcule distance euclidienne :
    for i in range (tab_proto1.shape[0]):
        tab_proto2[i] = sqrt((proto2[0]-proto2[1])**2 + (data[i,0]-data[i,1])**2)

    # Creation de cluster :
    Q = np.zeros((data.shape[0], data.shape[1]))
    for i in range (data.shape[0]) :
        print(tab_proto1[i], tab_proto2[i])
        if tab_proto1[i] < tab_proto2[i] :
            Q[i][0] = 1
        else:
            Q[i][1] = 1
    return(Q)


def K_means(data):
    Qbis = init(data)
    proto1, proto2 = initProto(Qbis)
    while round(proto1[0],1) != round(proto2[0],1) :
        Q = phase2(data, proto1, proto2)
        proto1, proto2 = initProto(Q)
    return proto1, proto2
    
print(K_means(data))