#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct  1 10:31:19 2017

@author: sarah
"""


""" Bibliothèque :"""
import numpy as np
import matplotlib.pyplot as plt
import os

os.chdir("/users/nfs/Etu5/3109515/MAPSI")

""" -------------------------- LA PLANCHE DE GALTON : --------------------------""" 


""" 1 - Loi de Bernoulli :"""

import random

def Loi_de_Bernoulli(p) :
    if random.random() < p :
        return (1, p)
    else :
        return (0, 1-p)
    
# Test : print(Loi_de_Bernoulli(0.5))

""" 2 - Loi Binomiale :"""

def Loi_binomiale(n, p) :
    return np.random.binomial(n, p, 1)[0]
    
# Test : print(Loi_binomiale(4, 3))
# Question : Je comprends pas la valeur aléatoire qui est retourné.

""" 3 - Histogramme de la loi binomiale:"""
n = 20
t_taille = 10000
tableau_1000_cases = np.zeros((1,t_taille), dtype=int)
for i in range(t_taille) :
    tableau_1000_cases[0][i] = Loi_binomiale(20, 0.5)
    

#plt.hist(tableau_1000_cases[0],  len(set(tableau_1000_cases[0])), edgecolor="red")



""" -------------------------- VISUALISATION D'INDEPENDANCES : --------------------------"""


""" 1 - Loi normale centrée :"""

def normale( k, sigma ):
    if k % 2 == 0:
        raise ValueError ( 'le nombre k doit etre impair' )
    x = np.linspace(-2*sigma, 2*sigma, k)
    y = np.random.normal(0, sigma**2, k)
    return y
        
#x = plt.hist(normale(10001,4))
#plt.plot(x[1][1:], x[0])

# Question : Qu'est qu'on a en abscisse, et qu'est-ce que retourne plt.hist ?


""" 2 - Loi normale centrée :"""

def proba_affine( k, slope ):
    if k % 2 == 0:
        raise ValueError ( 'le nombre k doit etre impair' )
    if abs ( slope  ) > 2. / ( k * k ):
        raise ValueError ( 'la pente est trop raide : pente max = ' + 
        str ( 2. / ( k * k ) ) )
    y = [i for i in range (k)]
    
# Question : Comment choisir les x ?

""" 3 - Distribution jointe: """

pA = np.array ( [0.2, 0.7, 0.1] )
pB = np.array ( [0.4, 0.4, 0.2] )
# Test :
#print(pA.shape, pB.shape)
#print(np.zeros((pA.shape[0],pB.shape[0])))

# Rq : Argument considéré => Tableau a une dimesion.
def Distribution_jointe(pA, pB) :
    pAB = np.zeros((pA.shape[0],pB.shape[0]))
    for i in range (pA.shape[0]) :
        for j in range (pB.shape[0]) :
            pAB[i][j] = pA[i]*pB[j]
    return pAB
    
# Test : 
#print(Distribution_jointe(pA, pB))



""" 4 -  Affichage de la distribution jointe:"""


from mpl_toolkits.mplot3d import Axes3D

def dessine ( P_jointe ):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    x = np.linspace ( -3, 3, P_jointe.shape[0] )
    y = np.linspace ( -3, 3, P_jointe.shape[1] )
    X, Y = np.meshgrid(x, y)
    ax.plot_surface(X, Y, P_jointe, rstride=1, cstride=1 )
    ax.set_xlabel('A')
    ax.set_ylabel('B')
    ax.set_zlabel('P(A) * P(B)')
    plt.show ()
    

""" -------------------------- INDEPENDANCES CONDITIONNELLES : --------------------------"""

P_XYZT = np.array([[[[ 0.0192,  0.1728],
                     [ 0.0384,  0.0096]],

                    [[ 0.0768,  0.0512],
                     [ 0.016 ,  0.016 ]]],

                   [[[ 0.0144,  0.1296],
                     [ 0.0288,  0.0072]],

                    [[ 0.2016,  0.1344],
                     [ 0.042 ,  0.042 ]]]])

# Test : 
#print(P_XYZT)


""" 1 -  Indépendance de X et T conditionnellement à (Y,Z):"""

""" P_YZ : """


print(P_XYZT.shape)
"""
P_YZ = np.zeros((2,2))
for y in range(P_XYZT.shape[0]) :
    for z in range(P_XYZT.shape[1]) : 
        sum = 0
        for x in range (P_XYZT.shape[2]) :
            sum += P_XYZT[x][y][z].sum()
        P_YZ[y][z] = sum
"""
print("\nP_YZ :")       
print(P_YZ)     

# Test :
"""  
print(P_XYZT[0][0][0])
print(P_XYZT[1][0][0])
print("\n\n")
print(P_XYZT[0][0][0].sum()+P_XYZT[1][0][0].sum())
print("\n\n")
print(P_XYZT[0][0][1])
print(P_XYZT[1][0][1])
print("\n\n")
print(P_XYZT[0][1][0])
print(P_XYZT[1][1][0])
print("\n\n")
print(P_XYZT[0][1][1])
print(P_XYZT[1][1][1])
"""

#t = P_XYZT.copy()
print("Debut")
print(P_XYZT[0][0][0][0])
print(P_XYZT[1][0][0][0])
print("\n\n")
print("\n\n")
print(P_XYZT[0][0][1][0])
print(P_XYZT[1][0][1][0])
print("\n\n")
print(P_XYZT[0][1][0])
print(P_XYZT[1][1][0])
print("\n\n")
print(P_XYZT[0][1][1])
print(P_XYZT[1][1][1])


P_YZ = np.zeros((2,2,2))
for t in range(P_XYZT.shape[3]) :
    for y in range(P_XYZT.shape[0]) :
        for z in range(P_XYZT.shape[1]) :
            sum = 0
            for x in range (P_XYZT.shape[2]) :
               sum += P_XYZT[x][y][z][t]
            P_YZ[y][z][t] = sum
print(P_YZ)











