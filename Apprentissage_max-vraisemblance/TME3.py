# -*- coding: utf-8 -*-
"""
Created on Fri Oct  6 22:25:12 2017

@author: Cassandre Leroy et Sarah Lachiheb
""" 

import os
os.chdir("C:\\Users\\sarah\\Desktop\\Master\\MAPSI\\TME3")

import numpy as np
import math
import matplotlib.pyplot as plt

""" _______________ 1. Préparation / visualisation _______________"""


def read_file ( filename ):
    """
    Lit un fichier USPS et renvoie un tableau de tableaux d'images.
    Chaque image est un tableau de nombres réels.
    Chaque tableau d'images contient des images de la même classe.
    Ainsi, T = read_file ( "fichier" ) est tel que T[0] est le tableau
    des images de la classe 0, T[1] contient celui des images de la classe 1,
    et ainsi de suite.
    """
    # lecture de l'en-tête
    infile = open ( filename, "r" )    
    nb_classes, nb_features = [ int( x ) for x in infile.readline().split() ]

    # creation de la structure de données pour sauver les images :
    # c'est un tableau de listes (1 par classe)
    data = np.empty ( 10, dtype=object )   
    filler = np.frompyfunc(lambda x: list(), 1, 1)
    filler( data, data )

    # lecture des images du fichier et tri, classe par classe
    for ligne in infile:
        champs = ligne.split ()
        if len ( champs ) == nb_features + 1:
            classe = int ( champs.pop ( 0 ) )
            data[classe].append ( list ( map ( lambda x: float(x), champs ) ) )
    infile.close ()

    # transformation des list en array
    output  = np.empty ( 10, dtype=object )
    filler2 = np.frompyfunc(lambda x: np.asarray (x), 1, 1)
    filler2 ( data, output )

    return output

def display_image ( X ):
    """
    Etant donné un tableau X de 256 flotants représentant une image de 16x16
    pixels, la fonction affiche cette image dans une fenêtre.
    """
    # on teste que le tableau contient bien 256 valeurs
    if X.size != 256:
        raise ValueError ( "Les images doivent être de 16x16 pixels" )

    # on crée une image pour imshow: chaque pixel est un tableau à 3 valeurs
    # (1 pour chaque canal R,G,B). Ces valeurs sont entre 0 et 1
    Y = X / X.max ()
    img = np.zeros ( ( Y.size, 3 ) )
    for i in range ( 3 ):
        img[:,i] = X

    # on indique que toutes les images sont de 16x16 pixels
    img.shape = (16,16,3)

    # affichage de l'image
    plt.imshow( img )
    plt.show ()
    
training_data = read_file ( "2015_tme3_usps_train.txt" )

# affichage du 1er chiffre "2" de la base:
#display_image ( training_data[2][0] )

# affichage du 5ème chiffre "3" de la base:
#display_image ( training_data[3][4] )

""" _______________ 2. Maximum de vraisemblance pour une classe _______________"""

def learnML_class_parameters(td) :
    nb_image = td.shape[0]
    nb_pixel = td.shape[1]
    esperance = np.zeros(nb_pixel)
    variance = np.zeros(nb_pixel)
    
    for pixel in range(nb_pixel): #256
        for image in range(nb_image): #1194
            esperance[pixel] += td[image][pixel]
        esperance[pixel] /= nb_image
    
    for id_esperance in range(nb_pixel):
        for image in range(nb_image):
            variance[id_esperance] += (td[image][id_esperance] - esperance[id_esperance])**2
        variance[id_esperance] /= nb_image
    return (esperance, variance)

print("\nMaximum de vraisemblance pour la classe 0:")
print(learnML_class_parameters(training_data[0]))
print("\nMaximum de vraisemblance pour la classe 1:")
print(learnML_class_parameters(training_data[1]))


""" _______________ 3. Maximum de vraisemblance pour toutes les classes _______________"""

def learnML_all_parameters(data) :
    parameters = []
    nb_image_total = training_data.shape[0]
    for i in range (nb_image_total):
        parameters.append(learnML_class_parameters(training_data[i]))
    return parameters 

parameters = learnML_all_parameters(training_data)   

""" _______________ 4. Log-vraisemblance d'une image _______________"""

test_data = read_file ( "2015_tme3_usps_test.txt" )
#print(test_data[2][3])
#print(parameters[0])

def log_likelihood (test_data, parameters_i) :
    som = 0
    for pixel ,(moy, var) in enumerate(zip(parameters_i[0], parameters_i[1])) :
        if var != 0.0 :
            som += (-1/2)*math.log(2*math.pi*var) - ((1/2)*((test_data[pixel]-moy)**2/var))
    return som

print("\nlog vraisemblance d'une image test_data[0][0]:")
print([ log_likelihood ( test_data[0][0], parameters[i] ) for i in range ( 10 ) ])

""" _______________ 5. Log-vraisemblance d une image (bis) _______________"""

def log_likelihoods(test_data, parameters) :
    tab = np.zeros(len(parameters))
    for i in range (tab.shape[0]) :
        tab[i] = log_likelihood (test_data, parameters[i])
    return tab

print("\nlog vraisemblance d'une image test_data[1][5](bis):")
print(log_likelihoods ( test_data[1][5], parameters ))

""" _______________ 6. Classification d'une image _______________"""

def classify_image (test_data, parameters) :
    tab = log_likelihoods(test_data, parameters)
    return tab.argmax()

print("\nClassification d'une image test_data[1][5]:")    
print(classify_image( test_data[1][5], parameters ))

print("\nClassification d'une image test_data[4][1]:")
print(classify_image( test_data[4][1], parameters ))


""" _______________ 7. Classification de toutes les images _______________"""
 
# test_data et parameters :
def classify_all_images (test_data, parameters) :
    classified_matrix = np.zeros((10,10))
    for i in range (test_data.shape[0]) :
        liste = []
        for j in range (test_data[i].shape[0]) :
            liste.append(classify_image( test_data[i][j], parameters ))
        for j in range (10) :
            classified_matrix[i][j] = liste.count(j)
            classified_matrix[i][j] /= len(liste)
    return classified_matrix

print("\nPourcentage d'image correspondant dans la réalité au chiffre 0 que notre classifieur à classer en 0:")
print(classify_all_images (test_data, parameters)[0][0])

""" _______________  8. Affichage du résultat des classifications _______________"""

from mpl_toolkits.mplot3d import Axes3D
import matplotlib

def dessine ( classified_matrix ):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    x = y = np.linspace ( 0, 9, 10 )
    X, Y = np.meshgrid(x, y)
    ax.plot_surface(X, Y, classified_matrix, rstride = 1, cstride=1 )
    
def dessine_imshow ( classified_matrix ):    
    img = matplotlib.pyplot.imshow(classified_matrix)
    matplotlib.pyplot.colorbar(img)


#dessine (classify_all_images (test_data, parameters))
dessine_imshow (classify_all_images (test_data, parameters))
















 