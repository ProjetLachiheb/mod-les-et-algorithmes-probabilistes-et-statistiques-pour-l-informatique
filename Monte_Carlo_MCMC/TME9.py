#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  8 13:37:33 2017

@author: Sarah
"""

import matplotlib.pyplot as plt
import numpy as np
import random
import math

######  Estimation de π par Monte Carlo :

def monteCarlo(nbIt) :
    d = 0
    x = np.random.uniform(-1, 1, nbIt)
    y = np.random.uniform(-1, 1, nbIt)

    for i in range(len(x)) :
        if x[i]**2 + y[i]**2 < 1 :
            d += 1
    return 4*d / nbIt, x, y

#np.sum(nbIt) equivaut à if x**2 + y**2 < 1 then d += 1
    #return (4.0 / (nbIt * np.sum(nbIt))), x, y


plt.figure()

# trace le carré
plt.plot([-1, -1, 1, 1], [-1, 1, 1, -1], '-')

# trace le cercle
x = np.linspace(-1, 1, 100)
y = np.sqrt(1- x*x)
plt.plot(x, y, 'b')
plt.plot(x, -y, 'b')

print('Estimation de π par Monte Carlo :')
pi, x, y = monteCarlo(int(1e4))

# trace les points dans le cercle et hors du cercle
dist = x*x + y*y
plt.plot(x[dist <=1], y[dist <=1], "go")
plt.plot(x[dist>1], y[dist>1], "ro")
plt.show()




###### Décodage par la méthode de Metropolis-Hastings


import pickle as pkl

# Question 1-2 :


with open('countWar1.pkl.txt', 'rb') as f:
    (count, mu, A) = pkl.load(f, encoding='latin1')
with open('fichierHash.pkl', 'rb') as f:
    chars2index = pkl.load(f, encoding='latin1')
secret = (open("secret.txt", "r")).read()[0:-1] # -1 pour supprimer le saut de ligne
secret2 = (open("secret2.txt", "r")).read()[0:-1] # -1 pour supprimer le saut de ligne

secret2 = list(secret2)


# Question 3 :


def swapF(dico) :
    res = dico.copy()
    k = dico.keys()
    chars = []
    for el in k :
        chars.append(el)
    l1 = random.randint(0, len(chars)-1)
    l2 = random.randint(0, len(chars)-1)
    while l1 == l2 :
        l2 = random.randint(0, len(chars)-1)
    c1 = chars[l1]
    c2 = chars[l2]
    res[c1] = dico[c2]
    res[c2] = dico[c1]
    return  res

tau = {'a' : 'b', 'b' : 'c', 'c' : 'a', 'd' : 'd' }
print("Question 3 :")
print(tau)
print(swapF(tau))


# Question 4 :


def decrypt(l, dico):
    res = []
    sol = []
    for el in l:
        res.append(dico[el])
    sol.append("".join(res))
    return sol[0]

print("\nQuestion 4 :")
print("Décodage de aabcd avec tau :")
print(decrypt ( "aabcd", tau ))
print("Décodage de dcba avec tau :")
print(decrypt ( "dcba", tau )   )


# Question 5 :

def  logLikelihood(seq, mu, A, k) :
    i = 0
    id0 = k[seq[0]]
    v = math.log(mu[id0])
    while i < len(seq)-1 :
        id1 = k[seq[i]]
        id2 = k[seq[i+1]]
        v += math.log(A[id1][id2])
        i += 1
    return v

print("\nQuestion 5 :")
print("Log-vraisemblance de abcd : ")
print(logLikelihood( "abcd", mu, A, chars2index))
print("Log-vraisemblance de dcba : ")
print(logLikelihood( "dcba", mu, A, chars2index))



# Question 6 :
def MetropolisHastings(mess, mu, A, tau, N) :
    i = 0
    while i < N :
        seq2 = decrypt(mess, tau)
        logOld = logLikelihood(seq2, mu, A, chars2index)

        tau_prime = swapF(tau)
        seq1 = decrypt(mess, tau_prime)
        logNew = logLikelihood(seq1, mu, A, chars2index)
        if math.log(random.uniform(0, 1)) <= min(0, logNew - logOld):
            tau =  tau_prime
        i += 1
    return decrypt(mess, tau_prime), logNew




def identityTau ():
    tau = {}
    for k in count.keys ():
        tau[k] = k
    return tau

seq_1 = []
for el in secret :
    for e in el :
        seq_1.append(e)
"""
#print()
#print(decrypt(seq_1, MetropolisHastings( secret2, mu, A, identityTau (), 10000 ) ))
print("avant")
print("".join(secret2))
print("après")
#a, b = MetropolisHastings( secret2, mu, A, identityTau (), 10000 )
print(b)
print("".join(a))
"""

 # Question 7 :

import numpy.random as npr

def updateOccurrences(text, count):
   for c in text:
      if c == u'\n':
         continue
      try:
         count[c] += 1
      except KeyError as e:
         count[c] = 1

def mostFrequent(count):
   bestK = []
   bestN = -1
   for k in count.keys():
      if (count[k]>bestN):
         bestK = [k]
         bestN = count[k]
      elif (count[k]==bestN):
         bestK.append(k)
   return bestK

def replaceF(f, kM, k):
   try:
      for c in f.keys():
         if f[c] == k:
            f[c] = f[kM]
            f[kM] = k
            return
   except KeyError as e:
      f[kM] = k

def mostFrequentF(message, count1, f={}):
   count = dict(count1)
   countM = {}
   updateOccurrences(message, countM)
   while len(countM) > 0:
      bestKM = mostFrequent(countM)
      bestK = mostFrequent(count)
      if len(bestKM)==1:
         kM = bestKM[0]
      else:
         kM = bestKM[npr.random_integers(0, len(bestKM)-1)]
      if len(bestK)==1:
         k = bestK[0]
      else:
         k = bestK[npr.random_integers(0, len(bestK)-1)]
      replaceF(f, kM, k)
      countM.pop(kM)
      count.pop(k)
   return f

tau_init = mostFrequentF(secret, count, identityTau () )
a, b = MetropolisHastings(secret, mu, A, tau_init, 50000 )
print(b)
print("".join(a))



""" SECRET 1
All human sciences have traveled along that path. Arriving at infinitesimals, mathematics, the most
 exact of sciences, abandons the process of analysis and enters on the new process of the integration
 of unknown, infinitely small, quantities. Abandoning the conception of cause, mathematics seeks law,
 that is, the property common to all unknown, infinitely small, elements. En another form but along
 the same path of reflection the other sciences have proceeded. Then Gewton enunciated the law of
 gravity he did not say that the sun or the earth had a property of attraction! he said that all
 bodies from the largest to the smallest have the property of attracting one another, that is,
 leaving aside the question of the cause of the movement of the bodies, he expressed the property
 common to all bodies from the infinitely large to the infinitely small. Whe same is done by the
 natural sciences?" leaving aside the question of cause, they seek for laws. Nistory stands on the
 same path. And if history has for its object the study of the movement of the nations and of
 humanity and not the narration of episodes in the lives of individuals, it too, setting aside
 the conception of cause, should seek the laws common to all the inseparably interconnected
 infinitesimal elements of free will.



"""

""" SECRET 2
 All human sciences have traveled along that path. Arriving at infinitesimals, mathematics, the most
 exact of sciences, abandons the process of analysis and enters on the new process of the integration of
 unknown, infinitely small, quantities. Abandoning the conception of cause, mathematics seeks law, that
 is, the property common to all unknown, infinitely small, elements. In another form but along the same
 path of reflection the other sciences have proceeded. Then Gewton enunciated the law of gravity he did
 not say that the sun or the earth had a property of attraction' he said that all bodies from the
 largest to the smallest have the property of attracting one another, that is, leaving aside the
 question of the cause of the movement of the bodies, he expressed the property common to all bodies
 from the infinitely large to the infinitely small. She same is done by the natural sciences; leaving
 aside the question of cause, they seek for laws. Pistory stands on the same path. And if history has
 for its object the study of the movement of the nations and of humanity and not the narration of
 episodes in the lives of individuals, it too, setting aside the conception of cause, should seek the
 laws common to all the inseparably interconnected infinitesimal elements of free will.
"""
