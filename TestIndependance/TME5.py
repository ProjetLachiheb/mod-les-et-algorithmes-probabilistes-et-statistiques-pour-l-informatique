# -*- coding: utf-8 -*-
"""
Created on Tue Oct 24 11:30:01 2017

@author: Sarah
"""


import numpy as np

# fonction pour transformer les données brutes en nombres de 0 à n-1
def translate_data ( data ):
    # création des structures de données à retourner
    nb_variables = data.shape[0]
    nb_observations = data.shape[1] - 1 # - nom variable
    res_data = np.zeros ( (nb_variables, nb_observations ), int )
    res_dico = np.empty ( nb_variables, dtype=object )

    # pour chaque variable, faire la traduction
    for i in range ( nb_variables ):
        res_dico[i] = {}
        index = 0
        for j in range ( 1, nb_observations + 1 ):
            # si l'observation n'existe pas dans le dictionnaire, la rajouter
            if data[i,j] not in res_dico[i]:
                res_dico[i].update ( { data[i,j] : index } )
                index += 1
            # rajouter la traduction dans le tableau de données à retourner
            res_data[i,j-1] = res_dico[i][data[i,j]]
    return ( res_data, res_dico )


# fonction pour lire les données de la base d'apprentissage
def read_csv ( filename ):
    data = np.loadtxt ( filename, delimiter=',', dtype=np.str ).T
    names = data[:,0].copy ()
    data, dico = translate_data ( data )
    return names, data, dico

# names : tableau contenant les noms des variables aléatoires
# data  : tableau 2D contenant les instanciations des variables aléatoires
# dico  : tableau de dictionnaires contenant la correspondance (valeur de variable -> nombre)
names, data, dico = read_csv ( "2015_tme5_asia.csv" )

#print(names,"\n", data, "\n", dico)

"""#--------------------------------------QUESTION1 : """

# etant donné une BD data et son dictionnaire, cette fonction crée le
# tableau de contingence de (x,y) | z
def create_contingency_table ( data, dico, x, y, z ):
    # détermination de la taille de z
    size_z = 1
    offset_z = np.zeros ( len ( z ) )
    j = 0
    for i in z:
        offset_z[j] = size_z
        size_z *= len ( dico[i] )
        j += 1

    # création du tableau de contingence
    res = np.zeros ( size_z, dtype = object )

    # remplissage du tableau de contingence
    if size_z != 1:
        z_values = np.apply_along_axis ( lambda val_z : val_z.dot ( offset_z ),
                                         1, data[z,:].T )
        i = 0
        while i < size_z:
            indices, = np.where ( z_values == i )
            a,b,c = np.histogram2d ( data[x,indices], data[y,indices],
                                     bins = [ len ( dico[x] ), len (dico[y] ) ] )
            res[i] = ( indices.size, a )
            i += 1
    else:
        a,b,c = np.histogram2d ( data[x,:], data[y,:],
                                 bins = [ len ( dico[x] ), len (dico[y] ) ] )
        res[0] = ( data.shape[1], a )
    return res

res = create_contingency_table ( data, dico, 0,1,[2,3] )
print("\n\n Resultats : \n", res)

"""#--------------------------------------QUESTION2 : """

def sufficient_statistics(data, dico, x, y, z) :
    tab = create_contingency_table (data, dico, x, y, z)
    #print("\n\n Resultats : \n", tab)
    valeurs_attendues = np.zeros(((len(tab), len(tab[1][1][0]), len(tab[1][1][1]))),float)

    i = 0
    for el in valeurs_attendues :
        if tab[i][0]  != 0:
            el[0][0] = ((tab[i][1][0][0] +  tab[i][1][0][1]) * (tab[i][1][1][0] + tab[i][1][0][0]))/ tab[i][0]
            el[0][1] = ((tab[i][1][0][1] +  tab[i][1][0][0]) * (tab[i][1][0][1] + tab[i][1][1][1]))/ tab[i][0]
            el[1][0] = ((tab[i][1][0][0] +  tab[i][1][1][0]) * (tab[i][1][1][1] + tab[i][1][1][0]))/ tab[i][0]
            el[1][1] = ((tab[i][1][0][1] +  tab[i][1][1][1]) * (tab[i][1][1][1] + tab[i][1][1][0]))/ tab[i][0]
        i += 1
    #print("\n\n Les valeurs attendues : \n",valeurs_attendues)
    res1= 0
    res2 = 0
    res3 = 0
    res4 = 0
    i = 0
    for el in tab :

        if valeurs_attendues[i][0][0] != 0 :
            res1 += ((el[1][0][0] - valeurs_attendues[i][0][0]) ** 2 ) / valeurs_attendues[i][0][0]
        if valeurs_attendues[i][0][1] != 0 :
            res2 += ((el[1][0][1] - valeurs_attendues[i][0][1]) ** 2 ) / valeurs_attendues[i][0][1]
        if valeurs_attendues[i][1][0] != 0 :
            res3 += ((el[1][1][0] - valeurs_attendues[i][1][0]) ** 2 ) / valeurs_attendues[i][1][0]
        if valeurs_attendues[i][1][1] != 0 :
            res4 += ((el[1][1][1] - valeurs_attendues[i][1][1]) ** 2 ) / valeurs_attendues[i][1][1]
        i += 1
    return res1 + res2 + res3 + res4

#print("\n\nQUESTION 2 :")
#print("X2 des valeurs 1,2,[3] : \n", sufficient_statistics ( data, dico, 1,2,[3]))
#print("\nX2 des valeurs 0,1,[2,3] : \n", sufficient_statistics ( data, dico, 0,1,[2,3]))
#print("\nX2 des valeurs 1,3,[2] : \n", sufficient_statistics ( data, dico, 1,3,[2]))
#print("\nX2 des valeurs 5,2,[1,3,6] : \n", sufficient_statistics ( data, dico, 5,2,[1,3,6]))
#print("\nX2 des valeurs 0,7,[4,5] : \n", sufficient_statistics ( data, dico, 0,7,[4,5]))
#print("\nX2 des valeurs 2,3,[5] : \n", sufficient_statistics ( data, dico, 2,3,[5]))


"""#--------------------------------------QUESTION3 : """

def sufficient_statistics2(data, dico, x, y, z) :
    tab = create_contingency_table (data, dico, x, y, z)
    #print("\n\n Resultats : \n", tab)
    valeurs_attendues = np.zeros(((len(tab), len(tab[0][1][0]), len(tab[0][1][1]))),float)
    # calcul des valeurs attentu pour ficilité le calcul de X2 :
    i = 0
    for el in valeurs_attendues :
        if tab[i][0]  != 0:
            el[0][0] = ((tab[i][1][0][0] +  tab[i][1][0][1]) * (tab[i][1][1][0] + tab[i][1][0][0]))/ tab[i][0]
            el[0][1] = ((tab[i][1][0][1] +  tab[i][1][0][0]) * (tab[i][1][0][1] + tab[i][1][1][1]))/ tab[i][0]
            el[1][0] = ((tab[i][1][0][0] +  tab[i][1][1][0]) * (tab[i][1][1][1] + tab[i][1][1][0]))/ tab[i][0]
            el[1][1] = ((tab[i][1][0][1] +  tab[i][1][1][1]) * (tab[i][1][1][1] + tab[i][1][1][0]))/ tab[i][0]
        i += 1
    #print("\n\n Les valeurs attendues : \n",valeurs_attendues)
    res1= 0
    res2 = 0
    res3 = 0
    res4 = 0
    i = 0
    for el in tab :
        if valeurs_attendues[i][0][0] != 0 :
            res1 += ((el[1][0][0] - valeurs_attendues[i][0][0]) ** 2 ) / valeurs_attendues[i][0][0]
        if valeurs_attendues[i][0][1] != 0 :
            res2 += ((el[1][0][1] - valeurs_attendues[i][0][1]) ** 2 ) / valeurs_attendues[i][0][1]
        if valeurs_attendues[i][1][0] != 0 :
            res3 += ((el[1][1][0] - valeurs_attendues[i][1][0]) ** 2 ) / valeurs_attendues[i][1][0]
        if valeurs_attendues[i][1][1] != 0 :
            res4 += ((el[1][1][1] - valeurs_attendues[i][1][1]) ** 2 ) / valeurs_attendues[i][1][1]
        i += 1
    #calcul des degrès de liberté, trouver où z != de 0 et si Nz est différent de 0:
    nbZ = 0
    for el in tab :
        if el[0] != 0 :
            nbZ += 1
    return (res1 + res2 + res3 + res4) , (tab[0][1].shape[0] - 1) * (tab[0][1].shape[1] - 1) * nbZ


print("\n\nQUESTION 3 :")
print("X2 des valeurs 1,2,[3] : \n", sufficient_statistics2 ( data, dico, 1,2,[3]))
print("\nX2 des valeurs 0,1,[2,3] : \n", sufficient_statistics2 ( data, dico, 0,1,[2,3]))
print("\nX2 des valeurs 1,3,[2] : \n", sufficient_statistics2 ( data, dico, 1,3,[2]))
print("\nX2 des valeurs 5,2,[1,3,6] : \n", sufficient_statistics2 ( data, dico, 5,2,[1,3,6]))
print("\nX2 des valeurs 0,7,[4,5] : \n", sufficient_statistics2 ( data, dico, 0,7,[4,5]))
print("\nX2 des valeurs 2,3,[5] : \n", sufficient_statistics2 ( data, dico, 2,3,[5]))
print("\nX2 des valeurs 1,3,[] : \n", sufficient_statistics2 ( data, dico, 1,3,[]))



"""#--------------------------------------QUESTION4 : """


import scipy.stats as stats

def indep_score(data, dico, x, y, z) :
    tab = create_contingency_table (data, dico, x, y, z)
    nbZ = 0
    for el in tab :
        if el[0] != 0 :
            nbZ += 1
    if len(data[0]) < 5 * tab[0][1].shape[0] *  tab[0][1].shape[1]  * nbZ:
        return (-1, 1)
    a, b = sufficient_statistics2(data, dico, x, y, z)
    return stats.chi2.sf (a, b)

print("\n\nQUESTION 4 :")
print(" p_value des valeurs 1,3,[] : \n", indep_score ( data, dico, 1,3,[]))
print("\n p_value des valeurs 1, 7, [] : \n", indep_score ( data, dico, 1, 7, []))
print("\n p_value des valeurs 0, 1,[2, 3] : \n", indep_score ( data, dico, 0, 1,[2, 3]))
print("\n p_value des valeurs 1, 2,[3, 4] : \n", indep_score ( data, dico, 1, 2,[3, 4]))


"""#--------------------------------------QUESTION5 : """

def best_candidate (data, dico, x, ez, a) :
    tab = []
    if x == 0 :
        return []
    for i in range(x) :
        tmp = indep_score(data, dico, x, i, ez)
        tab.append(tmp)
    m = min(tab)
    if m > a :
        return []
    else :
        return [tab.index(m)]


print("\n\nQUESTION 5 :")
print("Best Candidate 1, [], 0.05 : \n", best_candidate (data, dico,1, [], 0.05 ))
print("Best Candidate 4, [], 0.05 :\n", best_candidate (data, dico,4, [], 0.05 ))
print("Best Candidate  4, [1], 0.05:\n", best_candidate (data, dico, 4, [1], 0.05 ))
print("Best Candidate  5, [], 0.05:\n", best_candidate (data, dico, 5, [], 0.05 ))
print("Best Candidate 5, [6], 0.05:\n", best_candidate (data, dico,5, [6], 0.05 ))
print("Best Candidate 5, [6,7], 0.05 :\n", best_candidate (data, dico,5, [6,7], 0.05 ))



"""#--------------------------------------QUESTION6 : """

def create_parents ( data, dico, x, alpha ) :
    z = []
    while best_candidate (data, dico, x, z, alpha):
        z.append(best_candidate (data, dico, x, z, alpha )[0])
    return z


print("\n\nQUESTION 6 :")
print("Creation des noeuds parents  1, 0.05 : \n", create_parents ( data, dico, 1, 0.05 ))
print("Creation des noeuds parents  4, 0.05 : \n", create_parents ( data, dico, 4, 0.05 ))
print("Creation des noeuds parents  5, 0.05 : \n", create_parents ( data, dico, 5, 0.05 ))
print("Creation des noeuds parents  6, 0.05 : \n", create_parents ( data, dico, 6, 0.05 ))



"""#--------------------------------------QUESTION7 : """
def learn_BN_structure ( data, dico, alpha ) :
    res = []
    for i in range(len(data)) :
        res.append(create_parents ( data, dico, i, alpha ))
    tab = np.zeros((len(res)),dtype=list)
    i = 0
    for el in res :
        tab[i] = el
        i += 1
    print(tab)
    return tab, res



print("\n\nQUESTION 7 :")
x, listeN = learn_BN_structure ( data, dico, 0.05 )
print("Tableau contenant la liste de ses parents , pour chaque noeud :\n", listeN)


import pydotplus as pydot
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

style = { "bgcolor" : "#6b85d1", "fgcolor" : "#FFFFFF" }

def display_BN ( node_names, bn_struct, bn_name, style ):
    graph = pydot.Dot( bn_name, graph_type='digraph')

    # création des noeuds du réseau
    for name in node_names:
        new_node = pydot.Node( name,
                               style="filled",
                               fillcolor=style["bgcolor"],
                               fontcolor=style["fgcolor"] )
        graph.add_node( new_node )

    # création des arcs
    for node in range ( len ( node_names ) ):
        parents = bn_struct[node]
        for par in parents:
            new_edge = pydot.Edge ( node_names[par], node_names[node] )
            graph.add_edge ( new_edge )

    # sauvegarde et affaichage
    outfile = bn_name + '.png'
    graph.write_png( outfile )
    img = mpimg.imread ( outfile )
    plt.imshow( img )

display_BN (names, listeN, "asia", style )

#--------------------------------------QUESTION7 FIN :

# création du réseau bayésien à la aGrUM

import pyAgrum as gum
import pyAgrum.lib.ipython as gnb


def learn_parameters ( bn_struct, ficname ):
    # création du dag correspondant au bn_struct
    graphe = gum.DAG ()
    nodes = [ graphe.addNode () for i in range ( bn_struct.shape[0] ) ]
    for i in range ( bn_struct.shape[0] ):
        for parent in bn_struct[i]:
            graphe.addArc ( nodes[parent], nodes[i] )

    # appel au BNLearner pour apprendre les paramètres
    learner = gum.BNLearner ( ficname )
    learner.useScoreLog2Likelihood ()
    learner.useAprioriSmoothing ()
    return learner.learnParameters ( graphe )

ficname = "2015_tme5_asia.csv"


# création du réseau bayésien à la aGrUM
bn = learn_parameters ( x, ficname )

# affichage de sa taille
print("\nAffichage du réseau :\n", bn, "\n")

# récupération de la ''conditional probability table'' (CPT) et affichage de cette table
print("Affichage de la table de proba d'un noeud déterminé par son nom :")
gnb.showPotential( bn.cpt ( bn.idFromName ( 'bronchitis?' ) ) )

# calcul de la marginale
proba = gum.getPosterior ( bn, {}, 'bronchitis?' )
print("\nAffichage de la marginale : \n", proba)

# affichage de la marginale
print("\nAffichage graphique d'une ditribution de probabilité marginale :")
gnb.showPotential( proba )

#calcul d'une distribution marginale a posteriori : P(bronchitis? | smoking? = true, turberculosis? = false )
print("\nCalcul d'une distribution marginale a posteriori : P(bronchitis? | smoking? = true, turberculosis? = false )")
gnb.showPotential(gum.getPosterior ( bn,{'smoking?': 'true', 'tuberculosis?' : 'false' }, 'bronchitis?' ))
