#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 19 14:44:31 2017

"""

import numpy as np
import pickle as pkl
import matplotlib.pyplot as plt
import math
import random

# old version = python 2
# data = pkl.load(file("ressources/lettres.pkl","rb"))
# new : 
with open('/users/nfs/Etu5/3109515/MAPSI/TME6MAPSI/TME6_l.pkl.txt', 'rb') as f:
    data = pkl.load(f)#, encoding='latin1') 
X = np.array(data.get('letters')) # récupération des données sur les lettres
Y = np.array(data.get('labels')) # récupération des étiquettes associées

# affichage d'une lettre
def tracerLettre(let):
    a = -let*np.pi/180; # conversion en rad
    coord = np.array([[0, 0]]); # point initial
    for i in range(len(a)):
        x = np.array([[1, 0]]);
        rot = np.array([[np.cos(a[i]), -np.sin(a[i])],[ np.sin(a[i]),np.cos(a[i])]])
        xr = x.dot(rot) # application de la rotation
        coord = np.vstack((coord,xr+coord[-1,:]))
    plt.figure()
    plt.plot(coord[:,0],coord[:,1])
    plt.savefig("exlettre.png")
    return


#----------------Apprentissage d'un modèle CM (max de vraisemblance)

############## Q1 - Discrétisation : 
def discretise(X,d) :
    intervalle = 360. / d
    res = []
    for x in X :
        res.append(np.floor(x/intervalle))
    return res
    
print("Affichage de la question 1 : ")
print(X[7])
print(discretise(X,7)[7], "\n")



############## Q2 - Regrouper les indices des signaux par classe : 
def groupByLabel( y):
    index = []
    for i in np.unique(y): # pour toutes les classes :
        ind, = np.where(y==i)
        index.append(ind)
    return index

print("Affichage de la question 2 : ")
print("\n", groupByLabel(Y)) 



############## Q3 - Apprendre les modèles CM :     
def learnMarkovModel(Xc, d) :
    A = np.zeros((d,d)) # plus mauvais avec ones
    Pi = np.zeros(d) # plus mauvais avec ones
    # on définit Pi :
    for i in range(d) : 
        for el in Xc : 
            if el[0] == i : 
                Pi[i] += 1
    # On définit A : 
    for i in range(d):
        for j in range(d):
            for el in Xc :
                for k in range(len(el)-1):
                    if el[k] == i and el[k+1] == j : 
                        A[i][j] += 1  
    A = A/np.maximum(A.sum(1).reshape(d,1),1) # normalisation
    Pi = Pi/Pi.sum()
    return Pi, A 

def donne_Xc(X, Y, c) :
    tmp = []
    res = []
    for el in groupByLabel(Y)[c] : 
        tmp.append(el)
    for el in tmp : 
        res.append(X[el])
    return res
    
Xc = discretise(donne_Xc(X,Y,0),3)
print("Affichage de la question 3 : ")
print("Pi : ", learnMarkovModel(Xc, 3)[0])
print("A : ", learnMarkovModel(Xc, 3)[1])



############## Q4 - Stocker dans une liste : 
d=3     # paramètre de discrétisation
Xd = discretise(X,d)  # application de la discrétisation
index = groupByLabel(Y)  # groupement des signaux par classe
models = []
for cl in range(len(np.unique(Y))): # parcours de toutes les classes et optimisation des modèles
    Xc = discretise(donne_Xc(X,Y,cl),d)
    models.append(learnMarkovModel(Xc, d))  



#----------------Test (affectation dans les classes sur critère MV)

############## Q1 - logprobabilité d'une séquence dans un modèle : 

def probaSequence(s,Pi,A) : 
    res1 = 1
  #  i = 0
    for i in range(1,len(s)) :
        res1 *= A[int(s[i-1])][int(s[i])]
        
    if res1 == 0. or Pi[int(s[0])] == 0.:
        res1 = float('-inf')
    
    else :
        res1 = math.log(res1*Pi[int(s[0])])

    return res1

res = np.zeros(26)
for i in range(len(res)) : 
    res[i] = probaSequence(discretise(X[0], 3), models[i][0], models[i][1])
print("Affichage de la question 1 Partie 2 : \n", res)



############## Q2 - logprobabilité d'une séquence dans un modèle : 

proba = np.array([[probaSequence(Xd[i], models[cl][0], models[cl][1]) for i in range(len(Xd))]for cl in range(len(np.unique(Y)))])
print("\n\nAffichage de la question 2 Partie 2 : ")
print(proba)



############## Q3 - logprobabilité d'une séquence dans un modèle : 

Ynum = np.zeros(Y.shape)
for num,char in enumerate(np.unique(Y)):
    Ynum[Y==char] = num
    
pred = proba.argmax(0) # max colonne par colonne
A = np.where(pred != Ynum, 0.,1.).mean()
#print (np.where(pred != Ynum, 0.,1.).mean())
print("\n\nAffichage de la question 3 Partie 2 pour 3 états : ")
print(round(A*100))



#----------------Biais d'évaluation, notion de sur-apprentissage


# separation app/test, pc=ratio de points en apprentissage
def separeTrainTest(y, pc):
    indTrain = []
    indTest = []
    for i in np.unique(y): # pour toutes les classes
        ind, = np.where(y==i)
        n = len(ind)
        indTrain.append(ind[np.random.permutation(n)][:int(np.floor(pc*n))])

        indTest.append(np.setdiff1d(ind, indTrain[-1]))
    return indTrain, indTest
# exemple d'utilisation
itrain,itest = separeTrainTest(Y,0.8)

#print(itest)


############ OPTIONNELLE : Evaluation qualitative

def donne_XcBIS(X, itrain, c) :
    tmp = []
    res = []
    for el in itrain[c] : 
        tmp.append(el)
    for el in tmp : 
        res.append(X[el])
    return res

d=3     # paramètre de discrétisation
Xd = discretise(X,d)  # application de la discrétisation
modelsTrain = []
for cl in range(len(np.unique(Y))): # parcours de toutes les classes et optimisation des modèles
    Xc = discretise(donne_XcBIS(X,itrain,cl),d)
    modelsTrain.append(learnMarkovModel(Xc, d))  


conf = np.zeros((26,26))

ia = []
for i in itrain:
    ia += i.tolist()    
it = []
for i in itest:
    it += i.tolist()

proba = np.array([[probaSequence(Xd[it[i]], models[cl][0], models[cl][1]) for i in range(len(it))]for cl in range(len(np.unique(Y)))])

Ynum = np.zeros(Y.shape)
for num,char in enumerate(np.unique(Y)):
    Ynum[Y==char] = num

pred = proba.argmax(0) # max colonne par colonne

k = 0
for j in range(len(itest)) : 
    for i in range(len(itest[j])) :
        conf[pred[k]][j] += 1
        k += 1


plt.figure()
plt.imshow(conf, interpolation='nearest')
plt.colorbar()
plt.xticks(np.arange(26),np.unique(Y))
plt.yticks(np.arange(26),np.unique(Y))
plt.xlabel(u'Vérité terrain')
plt.ylabel(u'Prédiction')
plt.savefig("mat_conf_lettres.png")


######################" Générations : 



def tirage(A, etatAnt) :
    # Somme cumulée de la loi sc| :
    Abis = A[etatAnt].cumsum()

    # Tirer un nb aléa entre 0 et 1 :
    t = random.random()
    # Trouver la première valeure supérieur à t et retrouner cette état : 
    etat = 0
    for i in range (len(Abis)) :
        if Abis[i] > t :
            etat = i            
            break
    return etat

def generate(Pi,A, val): 
    etats = []

    # Calcul de s0 :
    Pib = Pi.cumsum()
    # Tirer un nb aléa entre 0 et 1 :
    t = random.random()
    # Trouver la première valeure supérieur à t et retrouner cette état : 
    etatInit = 0
    for i in range (len(Pib)) :
        if Pib[i] > t :
            etatInit = i            
            break
    etats.append(etatInit)
    for i in range(1,val - 1) : 
        etats.append(tirage(A, etats[i-1])) 
    return etats


print("\n")
            
newa = generate(models[0][0],models[0][1], 25) # generation d'une séquence d'états
intervalle = 360./d # pour passer des états => valeur d'angles
newa_continu = np.array([i*intervalle for i in newa]) # conv int => double
tracerLettre(newa_continu)

########################## Taux de reconnaissance en fonction de la discrétisation : 

def taux(X, Y, d) :
    Xd = discretise(X,d)  # application de la discrétisation
    models = []
    for cl in range(len(np.unique(Y))): # parcours de toutes les classes et optimisation des modèles
        Xc = discretise(donne_Xc(X,Y,cl),d)
        models.append(learnMarkovModel(Xc, d))  

    proba = np.array([[probaSequence(Xd[i], models[cl][0], models[cl][1])
    for i in range(len(Xd))]for cl in range(len(np.unique(Y)))])

    
    Ynum = np.zeros(Y.shape)
    for num,char in enumerate(np.unique(Y)):
        Ynum[Y==char] = num
    
    pred = proba.argmax(0) # max colonne par colonne
    to = np.where(pred != Ynum, 0.,1.).mean()
    return (round(to*100))
  

#print(taux(X,Y,9))
  

liste = []
for i in range(3,21):
    liste.append((taux(X,Y,i), i))
    
print("Liste des taux en fonction de d (taux, d)")    
print(liste)














