#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 23 20:47:07 2017

@author: sarah1
"""

import requests
import pickle as pkl
import time

url = "https://api.jcdecaux.com/vls/v1/stations?contract=Paris&apiKey=5efb0f3f43ccdba42bbc4c2e2c926ee86372fa2f"
dataStation = requests.get(url)
data = dataStation.json()
"""
urlGoogleAPI = "https://maps.googleapis.com/maps/api/elevation/json?locations="

for s in data:
    position = "%f,%f"%(s['position']['lat'],s['position']['lng'])
    alt = requests.get(urlGoogleAPI+position)
    assert(alt.json()['status'] == "OK") # verification de la réussite
    s[u'alt'] = alt.json()['results'][0]['elevation'] # enrichissement
    time.sleep(0.1) # pour ne pas se faire bannir par Google

f= open('coordVelib.pkl','wb')
pkl.dump(data,f) # penser à sauver les données pour éviter de refaire les opérations
f.close()
"""

datafinal = []
for station in data:
    a = station['number']
    ar = int(a)//1000
    if ar <= 20:
       datafinal.append(station)

arron = []
for station in datafinal:
    ar = station['number']//1000
    arron.append(ar)

pAR = []
nb = 0
for i in range(1,21) :
    nb=arron.count(i)
    pAR.append([i, (nb/len(arron))])

print(pAR)
