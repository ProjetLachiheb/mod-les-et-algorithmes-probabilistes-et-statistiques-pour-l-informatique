#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 22 18:02:06 2017

@author: sarah
"""

import numpy as np
import pickle as pkl
import matplotlib.pyplot as plt
import math
import numpy.linalg as alg

# truc pour un affichage plus convivial des matrices numpy
np.set_printoptions(precision=2, linewidth=320)
plt.close('all')

with open('TME6_lettres.pkl.txt', 'rb') as f:
    data = pkl.load(f, encoding='latin1')
X = np.array(data.get('letters')) # récupération des données sur les lettres
Y = np.array(data.get('labels')) # récupération des étiquettes associées


nCl = 26


def discretise(X,d) :
    intervalle = 360 / d
    res = []
    for x in X :
        res.append(np.floor(x/intervalle))
    return res

def groupByLabel( y):
    index = []
    for i in np.unique(y): # pour toutes les classes :
        ind, = np.where(y==i)
        index.append(ind)
    return index

def donne_Xc(X, Y, c) :
    tmp = []
    res = []
    for el in groupByLabel(Y)[c] :
        tmp.append(el)
    for el in tmp :
        res.append(X[el])
    return res

def donne_Xc(X, Y, c) :
    tmp = []
    res = []
    for el in groupByLabel(Y)[c] :
        tmp.append(el)
    for el in tmp :
        res.append(X[el])
    return res

##### Hypothèse Droite Gauche :

def initGD(X,N):
    res = []
    for x in X :
        res.append(np.floor(np.linspace(0,N-.00000001,len(x))))
    return res

##### Apprentissage :

def learnHMM(allx, allq, N, K, initTo0=False):
    if initTo0:
        A = np.zeros((N,N))
        B = np.zeros((N,K))
        Pi = np.zeros(N)
    else:
        eps = 1e-8
        A = np.ones((N,N))*eps
        B = np.ones((N,K))*eps
        Pi = np.ones(N)*eps

    # on définit Pi :
    for i in range(N) :
        for el in allq :
            if el[0] == i :
                Pi[i] += 1
    # On définit A :
    for i in range(N):
        for j in range(N):
            for el in allq :
                for k in range(len(el)-1):
                    if el[k] == i and el[k+1] == j :
                        A[i][j] += 1
    # On définit B :
    for i in range(N):
        for j in range(K):
            for m in range(len(allx)):
                for k in range(len(allx[m])) :
                    if q[m][k] == i and allx[m][k] == j :
                        B[i][j] += 1


    B = B/np.maximum(B.sum(1).reshape(N,1),1) # normalisation de B
    A = A/np.maximum(A.sum(1).reshape(N,1),1) # normalisation de A
    Pi = Pi/Pi.sum()
    return Pi, A, B


K = 10 # discrétisation (=10 observations possibles)
N = 5  # 5 états possibles (de 0 à 4 en python)
classe = 0
Xd = discretise(X, K)
Xc = discretise(donne_Xc(X,Y,classe),K)

print("Question 1 : ")
q = initGD(X,4)
print("X0 : ", Xd[0])
print("S0 : ", q[0])
q = initGD(Xc,N)
Pi, A, B = learnHMM(Xc,q,N,K, True)

print("\n Pi :")
print(Pi)
print("\n A :")
print(A)
print("\n B :")
print(B)

Pi, A, B = learnHMM(Xc,q,N,K)

##### Viterbi (en log) :


def viterbi(x, Pi, A, B):
    # Initialisations :
    nbEtats = len(A) #N
    nbE = len(x)
    best_s = np.zeros(nbE)
    p_est = 1
    q_est = 0
    viterbi = np.zeros((nbE,nbEtats)) # initialise viterbi table
    viterbi2 = np.zeros((nbE,nbEtats))

    for i in range(nbEtats) :
        viterbi[0][i] = math.log(Pi[i]) + math.log(B[i][int(x[0])])
        viterbi2[0][i] = -1
    #print(viterbi)

    # Récursion :
    for t in range(1,nbE):
        #indiceMAX = np.argmax(viterbi[t-1]) #np.amax(viterbi[t-1])
        for j in range(nbEtats):
            MAX1 = float('-inf')
            MAX2 = float('-inf')
            for i in range(nbEtats) :
                if (viterbi[t-1][i] + math.log(A[i][j])) > MAX1 :
                    MAX1 = viterbi[t-1][i] + math.log(A[i][j])
                if (viterbi[t-1][i] + math.log(A[i][j])) > MAX2 :
                    MAX2 = viterbi[t-1][i] + math.log(A[i][j])
                    val = i
            viterbi[t][j] = MAX1 + math.log(B[j][int(x[t])])
            viterbi2[t][j] = val

    p_est = np.amax(viterbi[t])
    q_est = np.amax(viterbi2[t])
    best_s[t] = q_est

    T = t-1
    while T > 0 :
        best_s[T] = viterbi2[T+1][int(best_s[T+1])]
        T -= 1

    return p_est, best_s


print("\nViterbi : ")  
a, b = viterbi(Xd[0], Pi, A, B)
print("s_est : ", a )
print("p_est : ", b )

"""
def viterbi2(x, Pi, A, B):
    # Initialisations :
    nbEtats = len(A) #N
    nbE = len(x)
    best_s = np.zeros(nbE)
    p_est = 1
    viterbi1 = np.zeros((nbE, nbEtats)) # initialise viterbi table
    viterbi2 = np.zeros((nbE, nbEtats)) # initialise viterbi table


    for i in range(nbEtats) :
        if Pi[i] == 0 or B[i][int(x[0])] == 0:
            viterbi1[0][i] = float('-inf')
        else :
            viterbi1[0][i] = math.log(Pi[i]) + math.log(B[i][int(x[0])])
        viterbi2[0][i] = -1

    print(viterbi1)
    print(viterbi2)


    # Récursion :
    for t in range(1,nbE):
        indiceMAX = st[len(st)-1].index(max(st[len(st)-1]))
        MAX1 = float('-inf')
        MAX2 = float('-inf')
        for j in range(0,nbEtats):
            if A[indiceMAX][j] == 0 or B[j][int(x[t])] == 0:
                viterbi1[t][j] = float('-inf')
            else :
                if max(np.viterbi1[t-1]) + math.log(A[indiceMAX][j]) > MAX1 :
                    MAX1 = max(st[len(st)-1]) + math.log(A[indiceMAX][j])
                if indiceMAX +  math.log(A[indiceMAX][j]) > MAX2 :
                    MAX2 = indiceMAX + math.log(A[indiceMAX][j])
                viterbi1[t][j] = MAX1 + math.log(B[j][int(x[t])])
                p.append(MAX2)
        st.append(s)

    for t in range(1,nbE):
        indiceMAX = st[t-1].index(max(st[t-1]))
        best_s[t-1] =  indiceMAX
    indiceMAX = st[t-1].index(max(st[t-1]))
    best_s[t] = indiceMAX

    p_est = max(st[t])

    return best_s, p_est


#print("\nViterbi2 : ")
#a, b = viterbi2(Xd[0], Pi, A, B)
#print("s_est : ", a )
#print("p_est : ", b )
"""


###### [OPT] Probabilité d'une séquence d'observation

def calc_log_pobs_v2(x,Pi, A, B) : #Forward
    nbEtats = len(A) #N
    nbE = len(x)
    alpha = np.zeros((nbE, nbEtats))


    for i in range(nbEtats) :
        alpha[0][i] = Pi[i] * B[i][int(x[0])]

    for t in range(1, nbE):
        for j in range (nbEtats):
            somme = 0
            for i in range(nbEtats):
                somme += (alpha[t-1][i] * A[i][j])
            alpha[t][j] = somme * B[j][int(x[t])]
    res = 0
    for el in alpha[t] :
        res += el
    return math.log(res)


p =  calc_log_pobs_v2(Xd[0],Pi, A, B)
print("\nRESULTAT version ALPHA : ", p)

def backward(x,Pi, A, B) :
    nbEtats = len(A) #N
    nbE = len(x)
    beta = np.zeros((nbE, nbEtats))
    T = nbE - 1

    for i in range(nbEtats):
        beta[T][i] = 1
    t = T
    while t > 0 :
        for i in range(nbEtats) :
            somme = 0
            for j in range(nbEtats):
                somme += (beta[t][j] * A[i][j] * B[j][int(x[t])])
            beta[t-1][i] = somme
        t -= 1
    res = 0
    for el in beta[0] :
        res += el

    res2 = 0
    for i in range(len(Pi)) :
        res2 +=  Pi[i] * beta[0][i]

    return math.log(res), res2



a, b =  backward(Xd[0],Pi, A, B)
print("\nRESULTAT version BETA : ",a)
print("RESULTAT version BETA * Pi[i] : ",math.log(b))
print("On ne trouve pas exactement pareil car on touche la précision machine mais on devrait sur papier")

##### Apprentissage complet (Baum-Welch simplifié)
classe = 0
Xd = discretise(X, K)
Xc = discretise(donne_Xc(X,Y,classe),K)

def Baum_Welch(X, Xc, q, N, K):
    x = initGD(Xc,N)
    k = 1
    val = float("inf")
    L = []
    Pi, A, B = learnHMM(Xc, q, N, K)
    p_est, s_est = viterbi(x, Pi, A, B)
    L.append(p_est)
    while (val < 0.00001) :
        for l in range(26):
            for i in range(len[x]):
                Pi[l], A[l], B[l] = learnHMM(Xc[i], q, N, K)
                p_est, s_est = viterbi(x, Pi, A, B)
                L.append(p_est)
        #Pi, A, B = learnHMM(Xc, q, N, K)
        #p_est, s_est = viterbi(x, Pi, A, B)
        #L.append(p_est)
        val = (L[k-1] - L[k]) / L[k-1]
    return L[k]



#print(Baum_Welch(X, Xc, q, N, K))



#----------------Biais d'évaluation, notion de sur-apprentissage


# separation app/test, pc=ratio de points en apprentissage
def separeTrainTest(y, pc):
    indTrain = []
    indTest = []
    for i in np.unique(y): # pour toutes les classes
        ind, = np.where(y==i)
        n = len(ind)
        indTrain.append(ind[np.random.permutation(n)][:int(np.floor(pc*n))])

        indTest.append(np.setdiff1d(ind, indTrain[-1]))
    return indTrain, indTest
# exemple d'utilisation
itrain,itest = separeTrainTest(Y,0.8)
